import initialState from './initialState';
import * as actions from '../actions/actionTypes';

export default function configReducer(
  state = initialState.config,
  action,
) {
  switch (action.type) {
    case actions.CHANGE_APP_TITLE:
      return {
        ...state,
        appTitle: action.appTitle,
      };

    case actions.DISPLAY_SELECTED_ORDERS:
      return {
        ...state,
        appTitle: 'Bin Putaway Review',
      };


    default:
      return state;
  }

}
