import initialState from './initialState';
import * as actions from '../actions/actionTypes';

export default function modalReducer(state = initialState.modals, action) {
  switch (action.type) {

    case actions.OPEN_LOCATION_SELECTOR:
      return {
        ...state,
        showLocationSelector: true
      };

    case actions.CLOSE_LOCATION_SELECTOR:
      return {
        ...state,
        showLocationSelector: false
      };

    case actions.LOCATION_CHANGED:
      return {
        ...state,
        showLocationSelector: !state.autoCloseLocationSelector
      };

    case actions.SHIP_ORDERS:
      return {
        ...state,
        showNotificationModal: true,
      };

    case actions.DSI_NOTIFICATION_CLOSE:
      return {
        ...state,
        showNotificationModal: false
      };


    default:
      return state;
  }
}