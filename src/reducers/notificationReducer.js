import initialState from './initialState';
import * as actions from '../actions/actionTypes';

export default function notificationReducer(state = initialState.notification, action) {
  switch (action.type) {
    case actions.CLOSE_NOTIFICATION:
      return {
        ...state,
        show: false
      };

    default:
      return state;
  }
};