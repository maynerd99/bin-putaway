import initialState from './initialState';
import * as actions from '../actions/actionTypes';

export default function locationReducer(state = initialState.locations, action) {
  switch (action.type) {

    case actions.GET_LOCATIONS:
      return action.locations;

    case actions.GET_LOCATIONS_SUCCESS:
      return action.locations;

    case actions.LOCATION_CHANGED:
      let locations = [...state];
      locations.forEach(function(location) {
        location.isDefault = location.id === Number(action.locationId);
      });
      return locations;

    default:
      return state;
  }
}