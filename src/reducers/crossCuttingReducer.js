import initialState from './initialState';
import * as actions from '../actions/actionTypes';

const crossCuttingReducer = (state, action) => {
  logAction(action);

  switch(action.type) {
    case actions.RESET_APPLICATION_STATE:
      return {
        ...initialState,
        locations: state.locations,
      };

    case actions.DSI_BACK_BUTTON_CLICKED:
      return getNavigateBackState(state);

    case actions.SUBMIT_FULFILLMENT_SUCCESS:
      return {
        ...state,
        ...processSaveFulfillmentResponse(state, action.response)
      };

    default:
      return state;
  }
};


function logAction(action) {
  const {type, ...payload} = action;
  console.log('type: ' + type, '- payload:', payload);
}

function listResponseElements(responseItem) {
  return responseItem + ',';
}

function processSaveFulfillmentResponse(state, response) {
  const successes = 'Success Ids: ' + response.arrSuccess.map(listResponseElements);
  const errors = 'Error Ids: ' + response.arrError.map(listResponseElements);
  const partialSuccess = !response.success && response.arrSuccess.length > 0 && response.arrError.length > 0;

  let message = response.success ? successes : errors;
  let title = response.isSuccess ? 'fulfillment successful' : 'fulfillment unsuccessful';
  if (partialSuccess) {
    title = 'partially successful';
    message = successes + '. ' + errors + '.';
  }

  return {
    notification: {
      ...state.notification,
      show: true,
      success: response.success,
      partialSuccess,
      autoClose: response.success,
      title,
      message,
      supplemental: response.success ? '' : 'See individual messages below'
    },
  }
}

function getNavigateBackState(state) {
  if (state.orderSelect.showOrderSelectScreen) {
    window.history.go(-1);
  } else if (state.orderReview.showReviewOrdersScreen) {
    return {
      ...state,
      orderSelect: {
        ...state.orderSelect,
        showOrderSelectScreen: true
      },
      orderReview: {
        ...state.orderReview,
        showReviewOrdersScreen: false
      },
      config: {
        appTitle: 'Bin Putaway',
      }
    };
  }
}


export default crossCuttingReducer;
