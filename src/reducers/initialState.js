// noinspection JSUnresolvedVariable
const showReview = window.nsConfig ? window.nsConfig.app.showReview : true;

export default {
  locations: [],
  config: {
    appTitle: 'Bin Putaway',
  },
  orderSelect: {
    orders: [],
    showOrderSelectScreen: true,
    showMultipleOrdersFoundModal: false,
    showReview: showReview,
    form: {
      ssccId: -1,
      ssccShow: false,
      fulfillmentId: -1,
      fulfillmentShow: false,
      salesOrderId: -1,
      salesOrderShow: false,
      orderNumber: null,
      scannedOrderNumber: '',
      isValidOrderNumber: true,
      setOrderNumberFocus: false,
    },
    allSelected: false,
    allExpanded: false,
    sort: {
      by: 'customer',
      ascending: true
    },
  },
  orderReview: {
    orders: [],
    showReviewOrdersScreen: false,
  },
  modals: {
    notificationTitle: 'Ship Complete',
    notificationMessage: 'All items have been shipped.',
    showLocationSelector: false,
    showNotification: true,
    showNotificationModal: false,
    autoCloseLocationSelector: true,
  },
  notification: {
    show: false,
    success: true,
    title: '',
    message: '',
    supplemental: '',
    autoClose: true,
    autoCloseTimeout: 4,
  },
  ajax: {
    callsInProgress: 0,
    loadingText: '',
  },
};
