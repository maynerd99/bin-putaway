import initialState from './initialState';
import * as actions from '../actions/actionTypes';

export default function orderSelectReducer(
  state = initialState.orderSelect,
  action,
) {
  switch (action.type) {
    case actions.GET_ORDERS_BY_LOCATION_SUCCESS:
      const successOrders = action.orders;
      successOrders.forEach(o => {
        o['status'] = 'incomplete';
        o['id'] = Number(o.soid);
        let packageCount = 0;
        let oshipmethod = "";
        o.fulfillments.forEach(
          fulfillment => {
            fulfillment['isSelected'] = false;
            let pkgCnt = isNaN(fulfillment.packagecount) ? 0 : Number(fulfillment.packagecount);
            packageCount = packageCount +  pkgCnt;
            oshipmethod = fulfillment.shipmethod;
            fulfillment['id'] = Number(fulfillment.fulfillmentid);
          }
        );
        o['packageCount'] = packageCount;
        o['shipmethod'] = oshipmethod;
        o['isSelected'] = o.fulfillments.length > 1;
      });

      return {
        ...state,
        orders: successOrders,
      };

    case actions.ORDER_CLICKED:
      const orders = [...state.orders];
      orders.forEach(
        o =>
          (o.isSelected =
            o.id === action.orderId ? !o.isSelected : o.isSelected),
      );
      return {
        ...state,
        orders,
      };

    case actions.ORDER_TAB_CLICKED:
      const tabs = { ...state.orderTabs };
      for (const tab in tabs) {
        tabs[tab] = tab === action.tab;
      }

      return {
        ...state,
        orderTabs: tabs,
      };

    case actions.DISPLAY_SELECTED_ORDERS:
      return {
        ...state,
        showOrderSelectScreen: false,
      }

    case actions.DSI_NOTIFICATION_CLOSE:
      return {
        ...state,
        showOrderSelectScreen: true
      };

    case actions.GET_ORDER_BY_TRANID_SUCCESS:
      const transOrder = action.orders[0];
      const existOrders = [...state.orders];
      if (transOrder.trantype === 'ItemShip') {
        existOrders.forEach(o =>
          o.fulfillments.map (f => f.fulfillmentid === transOrder.id ? f.isSelected = true: null));
      }
      else {
        let ord = null;
        existOrders.map(o => o.soid === transOrder.id ? ord = o : null);
        if (ord)
           ord.fulfillments.map (f => f.isSelected = true);
      }

      existOrders.forEach(o => {
          let numberSelected = 0;
          o.fulfillments.map (f => f.isSelected ? numberSelected = numberSelected + 1 : null);
          numberSelected === 0
            ? (o.status = 'incomplete')
            : numberSelected < o.fulfillments.length
            ? (o.status = 'partial')
            : (o.status = 'complete');
        } );

        return {
          ...state,
          orders: existOrders,
          form: {
            ...state.form,
            scannedOrderNumber: '',
          }
        };

    case actions.GET_ORDERS_BY_TRANID_SUCCESS:
      const transOrders = action.orders;
      let showSSCCButton = false;
      let ssccId = -1;
      let showFulfillmentButton = false;
      let fulfillmentId = -1;
      let showSalesOrderButton = false;
      let salesOrderId = -1;
      let showMultipleOrdersFoundModal = false;
      if (transOrders.length > 0) {
        showMultipleOrdersFoundModal = true;
        transOrders.forEach(o => {
          if (o.trantype === 'ItemShip') {
            if (o.isSSCC) {
              showSSCCButton = true;
              ssccId = o.id;
            }
            else {
              showFulfillmentButton = true;
              fulfillmentId = o.id;
            }
          }
          else {
            showSalesOrderButton = true;
            salesOrderId = o.id;
          }
        });
      }

      return {
        ...state,
        showMultipleOrdersFoundModal: showMultipleOrdersFoundModal,
        form: {
          ...state.form,
          scannedOrderNumber: '',
          ssccShow: showSSCCButton,
          ssccId: ssccId,
          fulfillmentShow: showFulfillmentButton,
          fulfillmentId: fulfillmentId,
          salesOrderShow: showSalesOrderButton,
          salesOrderId: salesOrderId,
        }
      };
    case actions.ORDER_NUMBER_CHANGED:
      return {
        ...state,
        form: {
          ...state.form,
          scannedOrderNumber: action.orderNumber
        }
      };

    case actions.MULTIPLE_DATA_TYPES_CLOSE:
      return {
        ...state,
        showMultipleOrdersFoundModal: false,
      };

    case actions.RECORD_TYPE_SELECTED:
      const lookupOrders = [...state.orders];
      const buttonType = action.recordType;
      const recordId = action.recordId;
      if (buttonType === 'salesOrder') {
        let rec = null;
        lookupOrders.map(o => o.soid === recordId ? rec = o : null);
        if (rec)
          rec.fulfillments.map (f => f.isSelected = true);
      }
      else
        lookupOrders.forEach(o =>
          o.fulfillments.map (f => f.fulfillmentid === recordId ? f.isSelected = true: null));

      lookupOrders.forEach(o => {
        let numberSelected = 0;
        o.fulfillments.map (f => f.isSelected ? numberSelected = numberSelected + 1 : null);
        numberSelected === 0
          ? (o.status = 'incomplete')
          : numberSelected < o.fulfillments.length
          ? (o.status = 'partial')
          : (o.status = 'complete');
      } );

      return {
        ...state,
        orders: lookupOrders,
        showMultipleOrdersFoundModal: false,
        form: {
          ...state.form,
          scannedOrderNumber: '',
        }
      };

    case actions.SELECT_ALL_FULFILLMENTS:
      const selectAllOrders = [...state.orders];
      const checked = action.checked;
      selectAllOrders.forEach(o => {
        o.fulfillments.forEach(f => f.isSelected = checked);
        checked ? o.status = 'complete' : o.status = 'incomplete' });
      return {
        ...state,
        orders: selectAllOrders,
        allSelected: checked,
      };

    case actions.EXPAND_ALL_ORDERS:
      const expandAllOrders = [...state.orders];
      const exchecked = action.checked;
      expandAllOrders.forEach(o => o.isSelected = exchecked);
      return {
        ...state,
        orders: expandAllOrders,
        allExpanded: exchecked,

      };

    case actions.FULFILLMENT_CLICKED:
      const clickedOrders = [...state.orders];
      let clickedOrder = clickedOrders.find(o => o.id === action.orderId);
      clickedOrder.fulfillments.forEach(f => {
        f.isSelected = f.id === action.fulfillmentId ? !f.isSelected : f.isSelected;
      });
      let numberSelected = 0;
      clickedOrder.fulfillments.map(
        fulfillment => (fulfillment.isSelected ? numberSelected++ : 0),
      );
      numberSelected === 0
        ? (clickedOrder.status = 'incomplete')
        : numberSelected < clickedOrder.fulfillments.length
          ? (clickedOrder.status = 'partial')
          : (clickedOrder.status = 'complete');

      return {
        ...state,
        orders: clickedOrders,
      };

    case actions.SORT_CLICKED:
      const sortedOrders = sortOrders(action.sort);
      return {
        ...state,
        orders: sortedOrders,
        sort: action.sort,
      };

    default:
      return state;
  }

  function sortOrders(sort) {
    const direction = sort.ascending ? 1 : -1;
    const orders = [...state.orders];
    return orders.sort(alphaNumericSort);

    function alphaNumericSort(a, b) {
      if (a[sort.by] < b[sort.by]) {
        return -1 * direction;
      }
      if (a[sort.by] > b[sort.by]) {
        return 1 * direction;
      }
      return 0;
    }
  }
}
