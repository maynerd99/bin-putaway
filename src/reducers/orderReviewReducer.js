import initialState from './initialState';
import * as actions from '../actions/actionTypes';

export default function orderReviewReducer(
  state = initialState.orderReview,
  action,
) {
  switch (action.type) {

    case actions.DISPLAY_SELECTED_ORDERS:
      const orders = action.orders;
      if (orders.length === 0) return {...state};
      let newOrds = [];
      orders.forEach(o => o.status !== 'incomplete' ? newOrds.push(o) : o);
      console.log(newOrds);

      return {
        ...state,
          showReviewOrdersScreen: true,
          orders: newOrds,
      };

    case actions.SHIP_ORDERS:
      return {
        ...state,
        showNotificationModal: true,
      };

    default:
      return state;

  }
}

