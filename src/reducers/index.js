import {combineReducers} from 'redux';
import reduceReducers from 'reduce-reducers';
import crossCuttingReducer from './crossCuttingReducer';
import modals from './modalReducer';
import locations from './locationReducer';
import ajax from './ajaxReducer';
import notification from './notificationReducer';
import orderSelect from './orderSelectReducer';
import orderReview from './orderReviewReducer';
import config from './configReducer';

const mainReducers = combineReducers({
  orderReview,
  orderSelect,
  config,
  ajax,
  modals,
  locations,
  notification,
});

export default reduceReducers(mainReducers, crossCuttingReducer);
