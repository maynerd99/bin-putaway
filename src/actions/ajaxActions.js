import * as actions from './actionTypes';

export function beginAjaxCall(message) {
  return {type: actions.BEGIN_AJAX_CALL, message };
}

export function ajaxCallError() {
  return {type: actions.AJAX_CALL_ERROR};
}