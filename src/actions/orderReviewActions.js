import { beginAjaxCall, ajaxCallError } from './ajaxActions';
import api from '../api';
import * as actions from './actionTypes';

export function shipOrders() {
  return { type: actions.SHIP_ORDERS };
}

export function pullAndSubmitFulfillments(orders) {
  let fulfillments = [];
  orders.forEach(o =>
    o.fulfillments.forEach(f => {
      let newObj = {};
      newObj['id'] = f.fulfillmentid;
      if (f.isSelected) fulfillments.push(newObj);
    }
  ));
  return function(dispatch) {
    dispatch(beginAjaxCall('saving fulfillments '));
    // noinspection JSUnresolvedFunction
    return api.submitFulfillmentsAjax(fulfillments)
      .then(response => !!response.json ? response.json() : response)
      .then(response => {
        dispatch(submitFulfillmentsSuccess(response));
      })
      .catch(error => {
        dispatch(ajaxCallError());
        throw (error);
      })
  };
}

export function submitFulfillmentsSuccess(response) {
  return { type: actions.SUBMIT_FULFILLMENT_SUCCESS, response };
}

