import api from '../api';
import {beginAjaxCall, ajaxCallError} from "./ajaxActions";
import * as actions from './actionTypes';
import { getOrdersByLocationAjax } from './orderSelectActions';

export function locationChanged(locationId) {
    return { type: actions.LOCATION_CHANGED, locationId};
}

export function getLocations(locations) {
  return { type: actions.GET_LOCATIONS, locations };
}

export function getLocationsSuccess(locations) {
  return { type: actions.GET_LOCATIONS_SUCCESS, locations }
}

export function locationHasChanged(locationId) {
  return function(dispatch) {
    dispatch(locationChanged(locationId));
    dispatch(getOrdersByLocationAjax(locationId));

  }
}

export function getUserLocations() {
  return function (dispatch) {
    const locations = window.locations || [];
    dispatch(getLocations(locations));
    dispatch(getLocationsSuccess(locations));
    const defaultLocation = locations.find(location => location.isDefault);
    if (defaultLocation) {
      console.log('Do something here if a default location is specified.');
    }
  }
}

export function getLocationsAjax() {
    return function(dispatch) {
      dispatch(beginAjaxCall('getting orders'));
      // noinspection JSUnresolvedFunction
      return api.getLocationsAjax()
        .then(locations => {
          dispatch(getLocationsSuccess(locations));
          const defaultLocation = locations.find(location => location.isDefault);
          if (defaultLocation) {
            dispatch(getOrdersByLocationAjax(defaultLocation.id));
          }
        })
        .catch(error => {
          dispatch(ajaxCallError(error));
          throw (error)
        });
    }
}


