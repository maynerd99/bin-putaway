import * as actions from './actionTypes';

export function  closeLocationSelector() {
  return { type: actions.CLOSE_LOCATION_SELECTOR };
}

export function openLocationSelector() {
  return { type: actions.OPEN_LOCATION_SELECTOR };
}


export const closeMultipleDataType = function() {
  return { type: actions.MULTIPLE_DATA_TYPES_CLOSE };
};

export const recordTypeChosen = function(recordId, recordType) {
  return { type: actions.RECORD_TYPE_SELECTED, recordId, recordType };
};