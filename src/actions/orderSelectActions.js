import { beginAjaxCall, ajaxCallError } from './ajaxActions';
import api from '../api';
import * as actions from './actionTypes';

export function getOrdersByLocationSuccess(orders) {
  return { type: actions.GET_ORDERS_BY_LOCATION_SUCCESS, orders };
}

export function getOrdersByTranIdAjaxSuccess(orders) {
  if (orders.length === 1)
    return { type: actions.GET_ORDER_BY_TRANID_SUCCESS, orders };
  else
    return { type: actions.GET_ORDERS_BY_TRANID_SUCCESS, orders };
}

export function orderClicked(orderId) {
  return { type: actions.ORDER_CLICKED, orderId };
}

export function orderNumberChanged(orderNumber) {
  return { type: actions.ORDER_NUMBER_CHANGED, orderNumber };
}

export function openOrderReviewScreen(orders) {
  return { type: actions.DISPLAY_SELECTED_ORDERS, orders };
}

export function fulfillmentClicked(orderId, fulfillmentId) {
  return { type: actions.FULFILLMENT_CLICKED, orderId, fulfillmentId };
}
export function sortClicked(sort) {
  return { type: actions.SORT_CLICKED, sort };
}

export function selectAllFulfillments(checked) {
  return { type: actions.SELECT_ALL_FULFILLMENTS, checked };
}

export function expandAllOrders(checked) {
  return { type: actions.EXPAND_ALL_ORDERS, checked };
}

export function getOrdersByTranIdAjax(tranId) {
  return function(dispatch) {
    dispatch(beginAjaxCall('finding an order'));
    // noinspection JSUnresolvedFunction
    return api.getOrdersByTranIdAjax(tranId)
      .then(response => !!response.json ? response.json() : response)
      .then(response => dispatch(getOrdersByTranIdAjaxSuccess(response.orders)))
      .catch(error => {
        console.log(error);
        dispatch(ajaxCallError());
      });
  };
}

export function getOrdersByLocationAjax(locationId) {
  return function(dispatch) {
    dispatch(beginAjaxCall('getting orders'));
    // noinspection JSUnresolvedFunction
    return api.getOrdersByLocationAjax(locationId)
      .then(response => !!response.json ? response.json() : response)
      .then(response => dispatch(getOrdersByLocationSuccess(response.orders)))
      .catch(error => {
        console.log(error);
        dispatch(ajaxCallError());
      });
  };
}
