import React, { Component } from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import * as actions from './NotificationModalActions';

import './NotificationModal.css';

class NotificationModal extends Component {

  componentDidMount() {
    if (this.props.autoClose) {
      const self = this;
      setTimeout(function() {
        if (self.props.onClose) {
          self.props.onClose();
        } else {
          self.onClose()
        }
      }, this.props.autoCloseTimeout * 1000)
    }
  }

  onClose = () => {
    this.props.dispatch(actions.closeNotification());
    this.props.dispatch(actions.resetApplicationState());
  };

  render() {
    return (
      <div id="notification-modal">
        <div id="notification-message">
          <h5 className={this.props.status}>{this.props.title}</h5>
          {this.props.message && <p>{this.props.message}</p>}
        </div>
        <div id="notification-close" onClick={this.props.onClose || this.onClose}>
          <i className="icon icon-swipe-up" />
          <p>close</p>
        </div>
      </div>
    );
  }
}

NotificationModal.defaultProps = {
  autoClose: true,
  autoCloseTimeout: 3,
  status: 'success'
};

NotificationModal.propTypes = {
  title: PropTypes.string.isRequired,
  message: PropTypes.string,
  autoClose: PropTypes.bool,
  autoCloseTimeout: PropTypes.number,
  onClose: PropTypes.func,
  status: PropTypes.oneOf(['success', 'failed'])
};

export default connect()(NotificationModal);