const DSI_NOTIFICATION_CLOSE = 'DSI_NOTIFICATION_CLOSE';
const RESET_APPLICATION_STATE = 'RESET_APPLICATION_STATE';

export const closeNotification = function() {
  return { type: DSI_NOTIFICATION_CLOSE };
};

export const resetApplicationState = function() {
  return { type: RESET_APPLICATION_STATE };
};
