import React from 'react';
import { connect } from 'react-redux';
import './OrderSelectScreen.css';
import MainForm from './MainForm/MainForm';
import SortSelector from './SortSelector/SortSelector';
import OrderList from './OrderList/OrderList';
import DSIFooter from '../common/DSIFooter/DSIFooter';
import DSIButton from '../common/DSIButton/DSIButton';
import * as actions from '../../actions/orderSelectActions';
import * as reviewActions from '../../actions/orderReviewActions';


const OrderSelectScreen = (props) => {

  const shipOrders = () => {
    props.dispatch(reviewActions.pullAndSubmitFulfillments(props.orders));
    props.dispatch(reviewActions.shipOrders());
  };
  const showReviewOrdersScreen = () => {
    props.dispatch(actions.openOrderReviewScreen(props.orders));
  };

  const orderClicked = (orderId) => {
    props.dispatch(actions.orderClicked(orderId));
  };

  const bottomButton = props.showReview ?  (
    <DSIButton label="review orders"
               onClick={showReviewOrdersScreen} />    ) : (
    <DSIButton label="ship orders"
               onClick={shipOrders} />  ) ;


  const orders = props.orders;
  let packageCounts = 0;
  orders.forEach(anOrder => packageCounts = packageCounts + anOrder.packageCount);


  return (
    <div id="order-select-screen">

      <MainForm />
      <SortSelector/>
      <OrderList orders={orders} cardClicked={orderClicked} chevronClicked={orderClicked} sort={props.sort}/>
      <div className="totals-grid">
        <div className="footer-titles">total orders</div>
        <div className="footer-amounts">{orders.length}</div>
        <div className="footer-titles">total boxes</div>
        <div className="footer-amounts">{packageCounts}</div>
      </div>
      <DSIFooter>
        {bottomButton}
      </DSIFooter>
    </div>
  )
};

OrderSelectScreen.propTypes = {

};

function mapStateToProps(state) {
  return {
    orders: state.orderSelect.orders,
    sort: state.orderSelect.sort,
    showReview: state.orderSelect.showReview,
    selectedLocation: state.selectedLocation,
  };
}

export default connect(mapStateToProps)(OrderSelectScreen);