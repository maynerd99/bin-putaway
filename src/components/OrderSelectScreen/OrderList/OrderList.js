import React from 'react';
import PropTypes from 'prop-types';
import './OrderList.css';
import DSIScrollContainer from '../../common/DSIScrollContainer/DSIScrollContainer';
import Accordion from '../../Accordion/Accordian';
import OrderCardContent from '../OrderCardContent/OrderCardContent';
import FulfillmentList from '../FulfillmentList/FulfillmentList';

const OrderList = props => {
  return (
    <DSIScrollContainer>
      {props.orders.map(order => (
          <Accordion
            key={order.id}
          >
            <OrderCardContent cardClicked={() => props.cardClicked(order.id)} chevronClicked={() => props.chevronClicked(order.id)} isSelected={order.isSelected} order={order} sort={props.sort} />
            <FulfillmentList key={order.id} order={order}  />
          </Accordion>
      ))}
    </DSIScrollContainer>
  );
};

OrderList.propTypes = {
  sort: PropTypes.shape({
    by: PropTypes.string.isRequired,
    ascending: PropTypes.bool.isRequired
  }).isRequired,
  orders: PropTypes.arrayOf(PropTypes.object),
  cardClicked: PropTypes.func,
  chevronClicked: PropTypes.func,
};

export default OrderList;
