import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import './SortSelector.css';
import * as actions from '../../../actions/orderSelectActions';
import SortItem from '../SortItem/SortItem';

const SortSelector = (props) => {
  const sortClicked = (sortBy) => {
    const sortAscending = sortBy === props.sort.by ? !props.sort.ascending : true;
    props.dispatch(actions.sortClicked({
      by: sortBy,
      ascending: sortAscending
    }));
  };

  return (
    <div id="sort-selector">
      <SortItem label="Customer"
                column="customer"
                sort={props.sort}
                onClick={sortClicked}/>

      <SortItem label="Order #"
                column="sonum"
                sort={props.sort}
                onClick={sortClicked}/>

      <SortItem label="Value"
                column="packageCount"
                sort={props.sort}
                onClick={sortClicked}/>

    </div>
  )
};

SortSelector.propTypes = {
  direction: PropTypes.bool
};

function mapStateToProps(state) {
  return {
    sort: state.orderSelect.sort
  };
}

export default connect(mapStateToProps)(SortSelector);