import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import './FulfillmentList.css';
import FulfillmentCardContent from '../FulfillmentCardContent/FulfillmentCardContent';


const FulfillmentList = props => {

  const listToDisplay = props.order.isSelected ? (
    <div>
      {props.order.fulfillments.map(fulfillment => (

        <FulfillmentCardContent key={fulfillment.id}
          fulfillment={fulfillment}
          oid={props.order.id}
          fid = {fulfillment.id}
        />
      ))}
    </div>
  ) : null;


  return <div>{listToDisplay}</div>;
};

FulfillmentList.propTypes = {
  orders: PropTypes.arrayOf(PropTypes.object),
  cardClicked: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    orders: state.orderSelect.orders,
  };
}

export default connect(mapStateToProps)(FulfillmentList);
