import React from 'react';
import PropTypes from 'prop-types';
import './FulfillmentCardContent.css';
import {connect} from "react-redux";
import * as actions from "../../../actions/orderSelectActions";


const FulfillmentCardContent = (props) => {

  const fulfillmentClicked = (oid, fid) => {
    props.dispatch(actions.fulfillmentClicked(oid, fid));
  };

  const selectedClasses = ['fulfillment-card', props.fulfillment.isSelected ? 'bold  handCursor' : 'normal  handCursor'].join(' ');

  const shipped = props.fulfillment.isSelected ?
    <i className="icon icon-blue-dot" /> : <div></div>;

  return (
    <div onClick={() => fulfillmentClicked(props.oid, props.fid)} className={selectedClasses} >
      {shipped}
      <div className="fulfillment-card-info">{props.fulfillment.fulfillment}</div>
      <div className="fulfillment-card-shipmethod tuncate">{props.fulfillment.shipmethod}</div>
      <div className="fulfillment-card-count">{props.fulfillment.packagecount}</div>
    </div>
  )
};

FulfillmentCardContent.propTypes = {
  fulfillment: PropTypes.shape({
    id: PropTypes.number.isRequired,
    fulfillment: PropTypes.string.isRequired,
    shipmethod: PropTypes.string.isRequired,
    packagecount: PropTypes.string.isRequired,
    isSelected: PropTypes.bool,
  }).isRequired,
  fulfillmentClicked: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    orders: state.orderSelect.orders,
  };
}

export default connect(mapStateToProps)(FulfillmentCardContent);
