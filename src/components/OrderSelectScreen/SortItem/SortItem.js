import React from 'react';
import PropTypes from 'prop-types';
import './SortItem.css';

const SortItem = (props) => {
  const sorted = props.column === props.sort.by;
  const sortClasses = sorted ? 'sort-selected  handCursor' : ' handCursor';
  const iconClasses = props.sort.ascending ? 'icon icon-sort-asc' : 'icon icon-sort-desc';

  return (
    <div className={sortClasses}
         onClick={() => props.onClick(props.column)}>
      {props.label}
      { sorted && <i className={iconClasses}/> }
    </div>
  )
};

SortItem.propTypes = {
  label: PropTypes.string.isRequired,
  column: PropTypes.string.isRequired,
  sort: PropTypes.shape({
    by: PropTypes.string.isRequired,
    ascending: PropTypes.bool.isRequired
  }).isRequired,
  onClick: PropTypes.func.isRequired,
};

export default SortItem;