import React from 'react';
import { connect } from 'react-redux';
import './MainForm.css';
import FormInput from "../../common/FormInput/FormInput";
import * as actions from '../../../actions/orderSelectActions';

const MainForm = (props) => {

  const onOrderNumberChanged = (e) => {
    props.dispatch(actions.orderNumberChanged(e.target.value));
  };

  const onOrderNumberBlur = (e) => {
    if (e.target.value.trim() === '') { return; }
    if (props.orders.length > 0)
      props.dispatch(actions.getOrdersByTranIdAjax(e.target.value.trim()));
  };

  const onSelectAllClick = () => {
    props.dispatch(actions.selectAllFulfillments(true));
  }

  const onSelectNoneClick = () => {
    props.dispatch(actions.selectAllFulfillments(false));
  }
  const onExpandAllClick = () => {
    props.dispatch(actions.expandAllOrders(true));
  }

  const onCollapseAllClick = () => {
    props.dispatch(actions.expandAllOrders(false));
  }

  const expandCollapse = !props.allExpanded ?
    (<div className="expand-commands handCursor" onClick={onExpandAllClick}>
      <i className="icon icon-expand-all"/>Expand All
    </div>) :
  (<div className="expand-commands handCursor" onClick={onCollapseAllClick}>
    <i className="icon icon-collapse-all"/>Collapse All
  </div>);

  const selectAllNone = !props.allSelected ?
    ( <div className="select-commands handCursor" onClick={onSelectAllClick}>
        <i className="icon icon-select-all"/>Select All
      </div> ) :
  (<div className="select-commands handCursor" onClick={onSelectNoneClick}>
    <i className="icon icon-select-none"/>Select None
  </div>);

  return (
    <div id="main-form">
      <FormInput name="order"
                 label="find an order"
                 placeholder="Scan / Enter Order, SSCC, or Fulfillment"
                 value={props.form.scannedOrderNumber}
                 onBlur={onOrderNumberBlur}
                 onChange={onOrderNumberChanged}
                 isValid={props.form.isValidOrderNumber}
                 setFocus={props.form.setOrderNumberFocus}
      />
      <div className="all-commands">
        {expandCollapse}
        {selectAllNone}
      </div>
    </div>
  )
};

function mapStateToProps(state) {
  return {
    form: state.orderSelect.form,
    orders: state.orderSelect.orders,
    allSelected: state.orderSelect.allSelected,
    allExpanded: state.orderSelect.allExpanded,
  };
}

export default connect(mapStateToProps)(MainForm);

