import React from 'react';
import PropTypes from 'prop-types';
import './OrderCardContent.css';

const OrderCardContent = (props) => {


  const cardClasses = ['order-card  handCursor'];
  if (props.order.status === 'complete') {
    cardClasses.push(' complete');
  }
  if (props.order.status === 'partial') {
    cardClasses.push(' partial');
  }
  if (props.order.status === 'incomplete') {
    cardClasses.push(' incomplete');
  }

  const iconClasses = props.order.isSelected ? 'icon icon-chevron-selected' : 'icon icon-chevron';
  const countClasses = ['sales-order-count', props.sort.by === 'packageCount' ? 'bold' : ''].join(' ');
  const customerClasses = ['customer', props.sort.by === 'customer' ? 'bold' : ''].join(' ');
  const orderClasses = ['sales-order', props.sort.by === 'soNumber' ? 'bold' : ''].join(' ');

  return (
    <div >
      <div className={cardClasses.join(' ')}>
        <div className="order-card-action">
          <i className={iconClasses} onClick={props.chevronClicked} />
        </div>
        <div className="order-card-info" onClick={props.cardClicked}>
          <div className={orderClasses}>{props.order.sonum}</div>
          <div className={countClasses}>{props.order.packageCount}</div>
          <div className={customerClasses}>{props.order.customer}</div>
        </div>
      </div>
    </div>
  );
};

OrderCardContent.propTypes = {
  sort: PropTypes.shape({
    by: PropTypes.string.isRequired,
    ascending: PropTypes.bool.isRequired
  }).isRequired,
  order: PropTypes.shape({
    id: PropTypes.number.isRequired,
    sonum: PropTypes.string.isRequired,
    customer: PropTypes.string.isRequired,
    packageCount: PropTypes.number.isRequired,
    fulfillments: PropTypes.arrayOf(PropTypes.object),
  }).isRequired,
};

export default OrderCardContent;
