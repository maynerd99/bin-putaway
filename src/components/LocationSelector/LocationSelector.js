import React, {Component} from 'react';
import DSILocation from '../common/DSILocation/DSILocation';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as locationActions from '../../actions/locationActions';
import * as modalActions from '../../actions/modalActions';
import DSIButton from '../common/DSIButton/DSIButton';
import './LocationSelector.css';

class LocationSelector extends Component{
  onLocationChange = (e) => {
    this.props.location.locationHasChanged(e.target.value);
  };

  onModalClick = (e) => {
    e.stopPropagation();
  };

  onClose = (e) => {
    this.props.modal.closeLocationSelector();
  };


  locations = this.props.locations;

  render() {

    return (
      <div id="location-selector-modal" onClick={this.onModalClick}>
        <div className="location-selector-header">
          <p>Change Location</p>
        </div>
        <div>
            <DSILocation name="location" id="location" onChange={this.onLocationChange} locations={this.locations}/>
        </div>
        <div className="location-selector-footer">
          <DSIButton label="close" onClick={this.onClose} />
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    locations: state.locations
  }
}

function mapDispatchToProps(dispatch) {
  return {
    location: bindActionCreators(locationActions, dispatch),
    modal: bindActionCreators(modalActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LocationSelector);