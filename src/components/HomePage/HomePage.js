import React, { Component } from 'react';
import { connect } from 'react-redux';
import Page from '../common/DSIPage/DSIPage';
import Header from '../common/DSIHeader/DSIHeader';
import './HomePage.css';
import '../../styles/dsi.appcommon.css';
import DSIModal from '../common/DSIModal/DSIModal';
import OrderSelectScreen from '../OrderSelectScreen/OrderSelectScreen';
import OrderReviewScreen from '../OrderReviewScreen/OrderReviewScreen';
import NotificationModal from '../NotificationModal/NotificationModal';
import Spinner from '../common/Spinner';
import MultipleDataTypesModal from '../MultipleDataTypesModal/MultipleDataTypesModal';
import LocationSelector from '../LocationSelector/LocationSelector'
import * as actions from '../../actions/modalActions';
class HomePage extends Component {

  closeMultipleDataTypeScreen = () => {
    this.props.dispatch(actions.closeMultipleDataType());
  };

  cardClickedMultipleDataTypeScreen = (id, selected) => {
    this.props.dispatch(actions.recordTypeChosen(id, selected));
  };


  render() {
    return (
      <Page>

        <Header appTitle={this.props.appTitle} />
        {this.props.showOrderSelectScreen && <OrderSelectScreen />}
        {this.props.showReviewOrdersScreen && <OrderReviewScreen />}

        <DSIModal
          show={this.props.modals.showNotificationModal}
          showCurtain={false}
          displayAt="top"
        >
          <NotificationModal
            title={this.props.modals.notificationTitle}
            message={this.props.modals.notificationMessage}
            status="success"
            autoClose={true}
          />
        </DSIModal>

        <DSIModal
          show={this.props.showMultipleOrdersFoundModal}
          showCurtain={true}
          displayAt="bottom"
        >
          <MultipleDataTypesModal
            showSSCCButton={this.props.showSSCCButton}
            showShipmentButton={this.props.showShipmentButton}
            showSalesOrderButton={this.props.showSalesOrderButton}
            ssccId={this.props.ssccId}
            fulfillmentId={this.props.fulfillmentId}
            salesOrderId={this.props.salesOrderId}

            onClose={this.closeMultipleDataTypeScreen}
            cardClicked={this.cardClickedMultipleDataTypeScreen}
          />
        </DSIModal>

        <DSIModal show={this.props.modals.showLocationSelector} displayAt="bottom" showCurtain={true}>
          <LocationSelector />
        </DSIModal>

        <Spinner
          isLoading={this.props.ajax.callsInProgress > 0}
          loadingText={this.props.ajax.loadingText}
        />
      </Page>
    );
  }
}

function mapStateToProps(state) {
  return {
    notification: state.notification,
    modals: state.modals,
    ajax: state.ajax,
    showSSCCButton: state.orderSelect.form.ssccShow,
    showShipmentButton: state.orderSelect.form.fulfillmentShow,
    showSalesOrderButton: state.orderSelect.form.salesOrderShow,
    ssccId: state.orderSelect.form.ssccId,
    fulfillmentId: state.orderSelect.form.fulfillmentId,
    salesOrderId: state.orderSelect.form.salesOrderId,
    showMultipleOrdersFoundModal: state.orderSelect.showMultipleOrdersFoundModal,
    showOrderSelectScreen: state.orderSelect.showOrderSelectScreen,
    showReviewOrdersScreen: state.orderReview.showReviewOrdersScreen,
    showNotificationModal: state.modals.showNotificationModal,
    appTitle: state.config.appTitle,
  };
}

export default connect(mapStateToProps)(HomePage);
