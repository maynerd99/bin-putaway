import React from 'react';
import PropTypes from 'prop-types';
import './Accordion.css';

const Accordion = props => {

  return (
    <div className="accordion">
      <div>
        {props.children}
      </div>
    </div>
  );
};

Accordion.defaultProps = {
  isSelected: false,
};

Accordion.propTypes = {
  isSelected: PropTypes.bool,
};

export default Accordion;
