import React from 'react';
import PropTypes from 'prop-types';
import './MultipleDataTypesModal.css';
import DSIButton from '../common/DSIButton/DSIButton';
import DSICard from '../common/DSICard/DSICard';
import DSIFooter from '../common/DSIFooter/DSIFooter';

const MultipleDataTypesModal = props => {

  const salesOrder = props.showSalesOrderButton ? (
    <DSICard cardClicked={() => props.cardClicked(props.salesOrderId, 'salesOrder')}>
      <div className="data-type-card  handCursor">Order #</div>
    </DSICard>
  ) : null;

  const shipment = props.showShipmentButton ? (
    <DSICard cardClicked={() => props.cardClicked(props.fulfillmentId, 'itemship')}>
      <div className="data-type-card  handCursor">Fulfillment #</div>
    </DSICard>
  ) : null;

  const sscc = props.showSSCCButton ? (
    <DSICard cardClicked={() => props.cardClicked(props.ssccId, 'sscc')}>
      <div className="data-type-card  handCursor">SSCC #</div>
    </DSICard>
  ) : null;

  return (
    <div id="multiple-data-types-modal">
      <div className="data-type-header">
        <div className='title'>Multiple Data Types Found</div>
        <div className='message'>The number you entered is associated with more than one data type. Select from the list below.</div>
      </div>
      <div className="scroll-container">
        {salesOrder}
        {shipment}
        {sscc}
      </div>
      <DSIFooter>
        <DSIButton label="close" onClick={props.onClose} />
      </DSIFooter>
    </div>
  );
};

MultipleDataTypesModal.propTypes = {
  onClose: PropTypes.func,
  showSSCCButton: PropTypes.bool,
  showShipmentButton: PropTypes.bool,
  showSalesOrderButton: PropTypes.bool,

};

export default MultipleDataTypesModal;
