import React from 'react';
import { connect } from 'react-redux';
import './OrderReviewScreen.css';
import OrderReviewList from './OrderReviewList/OrderReviewList';
import DSIFooter from '../common/DSIFooter/DSIFooter';
import DSIButton from '../common/DSIButton/DSIButton';
import * as actions from '../../actions/orderReviewActions';

const OrderReviewScreen = props => {
  const shipOrders = () => {
    props.dispatch(actions.pullAndSubmitFulfillments(props.orders));
    props.dispatch(actions.shipOrders());
  };

  const orders = props.orders;

  return (
    <div id="order-review-screen">
      <div></div>
      <OrderReviewList orders={orders} />
      <div className="review-totals-grid">
        <div className="review-footer-titles">total orders</div>
        <div className="review-footer-amounts">{orders.length}</div>
      </div>
      <DSIFooter>
        <DSIButton label="ship orders" onClick={shipOrders} />
      </DSIFooter>
    </div>
  );
};

OrderReviewScreen.propTypes = {};

function mapStateToProps(state) {
  return {
    orders: state.orderReview.orders,
  };
}

export default connect(mapStateToProps)(OrderReviewScreen);
