import React from 'react';
import PropTypes from 'prop-types';
import './OrderCardReview.css';

const OrderCardReview = ({ order }) => {

  const tot =  order.sonum.length + order.customer.length;
  const cardClass = tot < 35 ? 'order-review-card' : 'order-review-card2';

  return (
      <div className={cardClass}>
          <div className="review-order">{order.sonum}</div>
          <div className="review-customer">{order.customer}</div>
      </div>
  );
};

OrderCardReview.propTypes = {
  order: PropTypes.shape({
    id: PropTypes.number.isRequired,
    sonum: PropTypes.string.isRequired,
    customer: PropTypes.string.isRequired,
    shipmethod: PropTypes.string.isRequired,
    packageCount: PropTypes.number.isRequired,
  }).isRequired,
};

export default OrderCardReview;
