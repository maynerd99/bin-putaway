import React from 'react';
import PropTypes from 'prop-types';
import './OrderReviewList.css';
import DSIScrollContainer from '../../common/DSIScrollContainer/DSIScrollContainer';
import DSICard from '../../common/DSICard/DSICard';
import OrderReviewScreen from '../OrderCardReview/OrderCardReview';

const OrderReviewList = props => {
  return (
    <DSIScrollContainer>
      {props.orders.map(order =>
        <DSICard key={order.id}>
          <OrderReviewScreen order={order}/>
        </DSICard>
      )}
    </DSIScrollContainer>
  );
};

OrderReviewList.propTypes = {
  orders: PropTypes.arrayOf(PropTypes.object),
  cardClicked: PropTypes.func,
};

export default OrderReviewList;
