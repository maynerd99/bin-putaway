import React from 'react';
import PropTypes from 'prop-types';
import './DSIButton.css';

const DSIButton = (props) => {
  const buttonClasses = ['dsi-btn'];

  if (props.isSecondary) {
    buttonClasses.push('dsi-btn-secondary');
  }

  const onClick = (e) => {
    e.preventDefault();
    props.onClick(e);
  };

  return (
    <input type="button"
           className={buttonClasses.join(' ')}
           disabled={props.disabled}
           value={props.label}
           onClick={onClick}/>
  )
};

DSIButton.defaultProps = {
  isSecondary: false,
  disabled: false
};

DSIButton.propTypes = {
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  isSecondary: PropTypes.bool,
  disabled: PropTypes.bool
};

export default DSIButton;