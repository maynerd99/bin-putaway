import React from 'react';
import PropTypes from 'prop-types';
import './DSIPage.css';

const DSIPage = ({ children }) => (
    <div className="dsi-page">
        {children}
    </div>
);

DSIPage.propTypes = {
  children: PropTypes.arrayOf(PropTypes.any).isRequired
};

export default DSIPage;