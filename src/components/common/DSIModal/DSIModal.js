import React from 'react';
import PropTypes from 'prop-types';
import './DSIModal.css';

const DSIModal = (props) => {
  const curtainClasses = ['dsi-overlay'];
  curtainClasses.push(props.showCurtain ? 'dsi-overlay-curtain' : '');

  const modalClasses = ['dsi-modal', props.displayAt];

  return ( props.show ?
    <div className={curtainClasses.join(' ')} onClick={props.onCurtainClick}>
      <div className={modalClasses.join(' ')}>
        {props.children}
      </div>
    </div> : null
  )
};

DSIModal.defaultProps = {
  show: 'false',
  displayAt: 'bottom',
  showCurtain: true
};

DSIModal.propTypes = {
  show: PropTypes.bool,
  displayAt: PropTypes.oneOf(['top', 'center', 'bottom']),
  showCurtain: PropTypes.bool,
  onCurtainClick: PropTypes.func
};

export default DSIModal;