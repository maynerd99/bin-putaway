import React from 'react';
import './Spinner.css';

const spinner = (props) => {
    const loadingText = props.loadingText ? props.loadingText : 'processing';

    const loader = props.isLoading ?
        <div className='loader_container'>
            <div className='loader' />
            <div className='loader2' />
            <div className='loader_text'>{loadingText}</div>
        </div>
        : null;

    return (
        <div>
            {loader}
        </div>
    )

};

export default spinner;

/*
    <Spinner isLoading={<boolean>} loadingText='' />
*/
