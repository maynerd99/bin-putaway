import React from 'react';
import PropTypes from 'prop-types';
import './DSILocation.css';

const DSILocation = (props) => {
  const selectMessage = {
    id: 0,
    name: props.placeholder || 'Select a Location'
  };

  const locations = [ selectMessage, ...(props.locations || window.locations || []) ];
  const inputClasses = 'location-group-input' + (props.small ? ' input-small' : '');

  const defaultLocation = props.noDefault ? null : locations.find(location => location.isDefault);
  return (
    <div className="location-selector">
      <label htmlFor={props.name} className="location-group-label">{props.label || 'Location'}</label>
      <select
        className={inputClasses}
        name={props.name || 'location'}
        id={props.name || 'location'}
        onChange={props.onChange}
        defaultValue={defaultLocation ? defaultLocation.id : null}>
        {(locations || []).map((location) => {
          return <option key={location.id} value={location.id}>{location.name}</option>
        })}
      </select>
    </div>
  );
};

DSILocation.propTypes = {
  onChange: PropTypes.func.isRequired,
  name: PropTypes.string,
  label: PropTypes.string,
  noDefault: PropTypes.bool,
  locations: PropTypes.arrayOf(PropTypes.object),
  placeholder: PropTypes.string,
  locationValid: PropTypes.number
};

export default DSILocation;
