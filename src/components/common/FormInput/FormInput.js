import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './FormInput.css';

class FormInput extends Component {
 componentDidUpdate() {
   if (this.props.setFocus) {
     this.inputField.focus();
   }
 }

 render() {
   const hasListItems = (this.props.listItems && this.props.listItems.length > 0);
   const listId = hasListItems ? this.props.name + '-list' : null;
   const inputClasses = ['dsi-field-group-input'];

   const showOptions = typeof(this.props.onOptionsClick) === 'function';
   if (showOptions) {
     inputClasses.push('options');
   }
   return (
     <div className="dsi-field-group">
       <label htmlFor={this.props.name} className="dsi-field-group-label">
         {this.props.label}
         {this.props.additionalText && <span className="bold">{this.props.additionalText}</span>}
         </label>
       <input type="text"
              className={inputClasses.join(' ')}
              id={this.props.name}
              name={this.props.name}
              value={this.props.value}
              onChange={this.props.onChange}
              onBlur={this.props.onBlur}
              disabled={this.props.disabled}
              list={listId}
              ref={input => this.inputField = input}
              placeholder={this.props.placeholder}/>
       {showOptions && <i className="icon icon-three-dots" onClick={this.props.onOptionsClick}/>}
       {hasListItems &&
       <datalist id={listId}>
         { this.props.listItems.map(listItem => <option key={listItem.id} value={listItem.value}/>)}
       </datalist>
       }
     </div>
   );
 }
}

FormInput.defaultProps = {
  disabled: false,
  setFocus: false
};

FormInput.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  additionalText: PropTypes.string,
  placeholder: PropTypes.string,
  listItems: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    value: PropTypes.string.isRequired
  })),
  value: PropTypes.string,
  disabled: PropTypes.bool,
  setFocus: PropTypes.bool,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  onEnter: PropTypes.func,
  onOptionsClick: PropTypes.func
};

export default FormInput;
