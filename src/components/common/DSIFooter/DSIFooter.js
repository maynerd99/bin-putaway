import React from 'react';
import PropTypes from 'prop-types';
import './DSIFooter.css';

const DSIFooter = (props) => {
  const style={ backgroundColor: props.background };

  return (

    <div className="dsi-footer" style={style}>
      {props.children}
    </div>
  )
};

DSIFooter.defaultProps = {
  background: ''
};

DSIFooter.propTypes = {
  background: PropTypes.string,
  children: PropTypes.oneOfType([
  PropTypes.element,
  PropTypes.arrayOf(PropTypes.element)
]).isRequired
};

export default DSIFooter;