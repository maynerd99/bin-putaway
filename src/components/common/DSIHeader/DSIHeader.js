import React from "react";
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as modalActions from "../../../actions/modalActions";
import "./DSIHeader.css";

function handleMenuClick(e) {
  e.preventDefault();
  window.location =
    "/app/site/hosting/scriptlet.nl?script=customscript_dsi_app_menu&deploy=customdeploy_dsi_app_menu";
}

const DSIHeader = (props) => {
  const smallLocationName = props.defaultLocation.name.split(" : ").pop();
  return (
    <div className="dsi-header">
      <div className="dsi-header-navigation">
        <div onClick={props.backClicked}>
          <img className="dsi-header-back-icon" src={window.back || "/images/back.png"} alt="Go Back"/>
        </div>
        <div>
          <img className="dsi-header-logo-image" src={window.logo || "/images/logo.png"} alt="Logo"/>
        </div>
        <div>
          <div onClick={handleMenuClick}>
            <img className="dsi-header-menu-icon" src={window.menu || "/images/menu.png"} alt="DSI Menu"/>
          </div>
        </div>
      </div>

      <div className="dsi-header-app">
        <div className="dsi-header-app-title">
          {props.appTitle || window.appTitle  || 'Bin Putaway'}
        </div>
        <div className="dsi-header-app-location" onClick={props.actions.openLocationSelector}>
          <i className="icon icon-location"/>{smallLocationName}
        </div>
      </div>
    </div>
  );
};

DSIHeader.propTypes = {
  appTitle: PropTypes.string
};

function mapStateToProps(state) {
  let defaultLocation = state.locations.find(location => location.isDefault);

  if (!defaultLocation) {
    defaultLocation = { id: "0", name: "Select a Location" };
  }
  return {
    defaultLocation,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(modalActions, dispatch),
    backClicked: () => { dispatch({ type: 'DSI_BACK_BUTTON_CLICKED' }) }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DSIHeader);
