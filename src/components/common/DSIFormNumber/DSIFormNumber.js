import React from 'react';
import PropTypes from 'prop-types';
import './DSIFormNumber.css';

const DSIFormNumber = (props) => {

  return (
    <div className="dsi-number-group">
      <label>{props.label}</label>
      <div className="dsi-number-group-input">
        <div><i className="icon icon-minus" onClick={props.onClickMinus}/></div>
        <div><input type="number"
                    name={props.name}
                    id={props.id}
                    placeholder={props.placeholder}
                    value={props.value}
                    disabled={props.disabled}
                    onChange={props.onChange}
                    onBlur={props.onBlur}/>
        </div>
        <div><i className="icon icon-plus" onClick={props.onClickPlus}/></div>
      </div>
    </div>
  )
};

DSIFormNumber.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  value: PropTypes.number,
  onClickPlus: PropTypes.func.isRequired,
  onClickMinus: PropTypes.func.isRequired
};

export default DSIFormNumber;