import React from 'react';
import PropTypes from 'prop-types';
import './DSIScrollContainer.css';

const DSIScrollContainer = ({children}) => {

  return (
    <div className="dsi-scroll-container">
      {children}
    </div>
  )
};

DSIScrollContainer.propTypes = {
  children: PropTypes.oneOfType([
      PropTypes.element,
      PropTypes.arrayOf(PropTypes.element)
    ]).isRequired
};

export default DSIScrollContainer;