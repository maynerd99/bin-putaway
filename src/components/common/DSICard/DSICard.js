import React from 'react';
import PropTypes from 'prop-types';
import './DSICard.css';

const DSICard = (props) => {
  const cardClasses = ['dsi-card'];
  if (props.isSelected) {
    cardClasses.push('selected');
  }

  return (
    <div className={cardClasses.join(' ')} onClick={props.cardClicked}>
      {props.children}
    </div>
  )
};

DSICard.defaultProps = {
  isSelected: false
};

DSICard.propTypes = {
  isSelected: PropTypes.bool
};

export default DSICard;