import React, {Component} from 'react';
import {connect} from 'react-redux';
import DSIFooter from './common/DSIFooter';
import SubmitButton from './Submit_Button/Submit_Button';

class AppFooter extends Component {
  render() {
    return (
      <DSIFooter>
        <div className="footer-display">
          <div className="footer-text">TOTAL ORDERS</div>
          <div className="footer-text">6</div>
          <div className="footer-text">TOTAL BOXES</div>
          <div className="footer-text">1,525</div>
          <SubmitButton className="footer-button-text" text='REVIEW ORDERS'/>
        </div>
      </DSIFooter>
    );
  }
}

function mapStateToProps(state) {
  return { };
}

export default connect(mapStateToProps)(AppFooter);