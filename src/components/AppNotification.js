import React, {Component} from 'react';
import {connect} from 'react-redux';
import DSINotification from './common/DSINotification';
import * as actions from '../actions/notificationActions';

class AppNotification extends Component {

  onClose = () => {
    this.props.dispatch(actions.closeNotification());
    this.props.dispatch(actions.resetApplicationState());
  };

  render() {
    return (
      <DSINotification {...this.props.notification}
                       onClick={this.onClose}/>
    );
  }
}

function mapStateToProps(state) {
  return {
    notification: state.notification
  }
}

export default connect(mapStateToProps)(AppNotification);