import { applyMiddleware } from 'redux';
import { logger } from 'redux-logger';
import allEpics from '../epic';
import promise from 'redux-promise-middleware';
import error from './error';
import { composeWithDevTools } from 'redux-devtools-extension';

export default composeWithDevTools(applyMiddleware(allEpics, promise(), logger, error));