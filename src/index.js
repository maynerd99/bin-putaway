import 'babel-polyfill'
import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import App from './App';
import configureStore from './store/configureStore';
import * as locationActions from './actions/locationActions';

const store = configureStore();

if (window.locations) {
  store.dispatch(locationActions.getUserLocations());
} else {
  store.dispatch(locationActions.getLocationsAjax());
}

ReactDOM.render(
  <Provider store={store}>
    <App/>
  </Provider>,
  document.getElementById('root')
);
