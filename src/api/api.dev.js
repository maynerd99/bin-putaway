const api = {

  getOrdersByLocationAjax: () => {
    const orders = {
        "success": true,
        "orders": [{
          "fulfillments": [{
            "createdFrom": "SLS00000409",
            "fulfillment": "SLS00000415",
            "fulfillmentid": "927",
            "fulfillmemo": "",
            "fulfillamount": ".00",
            "shipmethodid": "",
            "shipmethod": "FedEx Overnite Express",
            "packagecount": "1",
            "shipzip": ""
          }, {
            "createdFrom": "SLS00000409",
            "fulfillment": "FUL00000005",
            "fulfillmentid": "928",
            "fulfillmemo": "",
            "fulfillamount": ".00",
            "shipmethodid": "",
            "shipmethod": "UPS",
            "packagecount": "1",
            "shipzip": ""
          }, {
            "createdFrom": "SLS00000409",
            "fulfillment": "FUL00000006",
            "fulfillmentid": "929",
            "fulfillmemo": "",
            "fulfillamount": ".00",
            "shipmethodid": "",
            "shipmethod": "UPS",
            "packagecount": "1",
            "shipzip": ""
          }],
          "saledate": "2/8/2018",
          "soid": "809",
          "sonum": "1234567890123456789012345678901234567890",
          "sotype": "SalesOrd",
          "shipdate": "2/8/2018",
          "salememo": "Telephone (4400) order",
          "saleamount": "2598.00",
          "customerid": "1720",
          "customer": "Clampett Farm"
        }, {
          "fulfillments": [{
            "createdFrom": "SLS00000415",
            "fulfillment": "FUL00000408",
            "fulfillmentid": "961",
            "fulfillmemo": "package the first",
            "fulfillamount": ".00",
            "shipmethodid": "",
            "shipmethod": "UPS",
            "packagecount": "1",
            "shipzip": ""
          }, {
            "createdFrom": "SLS00000415",
            "fulfillment": "FUL00000409",
            "fulfillmentid": "962",
            "fulfillmemo": "second package",
            "fulfillamount": ".00",
            "shipmethodid": "",
            "shipmethod": "UPS",
            "packagecount": "1",
            "shipzip": "",
          }, {
            "createdFrom": "SLS00000415",
            "fulfillment": "FUL00000410",
            "fulfillmentid": "963",
            "fulfillmemo": "ok to ship",
            "fulfillamount": ".00",
            "shipmethodid": "",
            "shipmethod": "UPS",
            "packagecount": "1",
            "shipzip": ""
          }],
          "saledate": "5/2/2018",
          "soid": "960",
          "sonum": "SLS00000415",
          "sotype": "SalesOrd",
          "shipdate": "5/4/2018",
          "salememo": "",
          "saleamount": "509.84",
          "customerid": "22",
          "customer": "3M"
        },
          {
            "fulfillments": [{
              "createdFrom": "SLS00000409",
              "fulfillment": "SLS00000415",
              "fulfillmentid": "829",
              "fulfillmemo": "",
              "fulfillamount": ".00",
              "shipmethodid": "",
              "shipmethod": "UPS",
              "packagecount": "1",
              "shipzip": ""
            }],
            "saledate": "2/8/2018",
            "soid": "709",
            "sonum": "SLS00000416",
            "sotype": "SalesOrd",
            "shipdate": "2/8/2018",
            "salememo": "Telephone (4400) order",
            "saleamount": "2598.00",
            "customerid": "1720",
            "customer": "Clampett Farm"
          }],
        "error": {
          "code": "",
          "message": ""
        }
      };

    return new Promise(resolve => {
      setTimeout(() => {
        console.log(orders);
        resolve(orders);
      }, 1000);
    })
  },
  getOrdersByTranIdAjax: (tranId) => {
    const orders = {
      "success":true,
      "error":{"code":"","message":""},
      "orders":[{
        "id":"927",
        "tranid":"FUL00000004",
        "trantype":"ItemShip",
        "fulfillment":"FUL00000004",
        "fulfillmentid":"927",
        "sonum":"SLS00000409",
        "soid":"809",
        "customer":"Clampett Farm",
        "customerid":"1720",
        "cartonid":"",
        "isSSCC":false},
        {
          "id":"960",
          "tranid":"FUL00000004",
          "trantype":"ItemShip",
          "fulfillment":"FUL00000004",
          "fulfillmentid":"927",
          "sonum":"SLS00000409",
          "soid":"809",
          "customer":"Clampett Farm",
          "customerid":"1720",
          "cartonid":"",
          "isSSCC":false}]
    };

  return new Promise(resolve => {
    setTimeout(() => {
      console.log(orders);
      resolve(orders);
    }, 1000);
  })
},

  submitFulfillmentsAjax: (fulfillments) => {
    const success = false;
    const response = success ? {
        "arrSuccess": [],
        "arrError": [],
        "success": true
      } :

      {
        "arrSuccess": ["961"],
        "arrError": ["971"],
        "success": false,
      };
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(response);
      }, 2000);
    })
  },

  getLocations: () => {
    return [
      {
        name: "01: San Francisco",
        id: 2
      }, {
        name: "01: San Francisco : QA Hold",
        id: 5
      }, {
        name: "01: San Francisco : Receiving Insp.",
        id: 4
      }, {
        name: "02: Boston",
        id: 1,
        "isDefault": true
      }, {
        name: "02: Boston : Overstock",
        id: 3
      }, {
        name: "HQ",
        id: 6
      }
    ]
  },

  getLocationsAjax: () => {
    const locations = [{
      name: "01: San Francisco",
      id: 2,
      isDefault: false
    }, {
      name: "01: San Francisco : QA Hold",
      id: 5
    }, {
      name: "01: San Francisco : Receiving Insp.",
      id: 4
    }, {
      name: "02: Boston",
      id: 1,
      isDefault: false
    }, {
      name: "02: Boston : Overstock",
      id: 3
    }, {
      name: "HQ",
      id: 6
    }];
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(locations);
      }, 1000);
    })
  },

  getBinsByLocationAjax: (locationId) => {
    const sanFranciscoBins = {
      bins: [
        {
          "id": "940",
          "binNumber": "\"Test\" Bin"
        }, {
          "id": "941",
          "binNumber": "'MDR' Bin"
        }, {
          "id": "6",
          "binNumber": "2001"
        }, {
          "id": "7",
          "binNumber": "2002"
        }, {
          "id": "8",
          "binNumber": "2003"
        }, {
          "id": "9",
          "binNumber": "2004"
        }, {
          "id": "10",
          "binNumber": "2005"
        }, {
          "id": "922",
          "binNumber": "2006"
        }, {
          "id": "925",
          "binNumber": "2007"
        }, {
          "id": "926",
          "binNumber": "2008"
        }, {
          "id": "17",
          "binNumber": "ABC123"
        }, {
          "id": "117",
          "binNumber": "Alpha-1"
        }, {
          "id": "118",
          "binNumber": "Alpha-2"
        }, {
          "id": "119",
          "binNumber": "Alpha-3"
        }
      ]
    };

    const bostonBins = {
      bins: [
        {
          "id": "1",
          "binNumber": "1001"
        },
        {
          "id": "2",
          "binNumber": "1002"
        },
        {
          "id": "3",
          "binNumber": "1003"
        },
        {
          "id": "4",
          "binNumber": "1004"
        },
        {
          "id": "5",
          "binNumber": "1005"
        },
        {
          "id": "11",
          "binNumber": "AC5510"
        },
        {
          "id": "12",
          "binNumber": "AC5511"
        },
        {
          "id": "13",
          "binNumber": "AC5512"
        },
        {
          "id": "14",
          "binNumber": "BC8801"
        },
        {
          "id": "15",
          "binNumber": "BC8802"
        },
        {
          "id": "16",
          "binNumber": "BC8803"
        },
        {
          "id": "127",
          "binNumber": "Beta-1"
        },
        {
          "id": "217",
          "binNumber": "Beta-2"
        },
        {
          "id": "218",
          "binNumber": "Beta-3"
        },
        {
          "id": "219",
          "binNumber": "Beta-4"
        },
        {
          "id": "220",
          "binNumber": "Beta-5"
        },
        {
          "id": "927",
          "binNumber": "J1000"
        },
        {
          "id": "928",
          "binNumber": "J1001"
        },
        {
          "id": "929",
          "binNumber": "J1002"
        },
        {
          "id": "930",
          "binNumber": "J1003"
        },
        {
          "id": "931",
          "binNumber": "J1004"
        },
        {
          "id": "932",
          "binNumber": "J1005"
        },
        {
          "id": "933",
          "binNumber": "J1006"
        },
        {
          "id": "420",
          "binNumber": "Rob-1"
        },
        {
          "id": "421",
          "binNumber": "Rob-2"
        },
        {
          "id": "422",
          "binNumber": "Rob-3"
        },
        {
          "id": "920",
          "binNumber": "SER4321"
        }
      ]
    };

    const bins = [null, bostonBins, sanFranciscoBins];
    const id = Number(locationId);

    return new Promise(resolve => {
      setTimeout(() => {
        resolve(bins[id]);
      }, 1000);
    });
  },

  getOrdersByBinAjax: () => {
    const orders = {
      orders: [
        {
          "id": 802,
          "tranid": "(00)012345678000000158",
          "trantype": "SalesOrd",
          "fulfillment": "",
          "fulfillmentid": "",
          "sonum": "(00)012345678000000158",
          "soid": "802",
          "customer": "Clampett Farm",
          "customerid": "1720",
          "cartonid": "",
          "isSSCC": false
        },
        {
          "id": "935",
          "tranid": "(00)012345678000000158",
          "trantype": "ItemShip",
          "fulfillment": "(00)012345678000000158",
          "fulfillmentid": "935",
          "sonum": "SLS00000412",
          "soid": "934",
          "customer": "Jesus Buys Everything",
          "customerid": "2424",
          "cartonid": "",
          "isSSCC": false
        },
        {
          "id": "935",
          "tranid": "(00)012345678000000158",
          "trantype": "ItemShip",
          "fulfillment": "(00)012345678000000158",
          "fulfillmentid": "935",
          "sonum": "SLS00000412",
          "soid": "934",
          "customer": "Jesus Buys Everything",
          "customerid": "2424",
          "cartonid": "14",
          "isSSCC": true
        }
      ]
    }

    return new Promise(resolve => {
      setTimeout(() => {
        resolve(orders);
      }, 1000);
    })
  }
}


export default api;