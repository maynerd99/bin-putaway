const api = {
  getLocations: () => {
    return [
      {
        name: "01: San Francisco",
        id: "2"
      }, {
        name: "01: San Francisco : QA Hold",
        id: "5"
      }, {
        name: "01: San Francisco : Receiving Insp.",
        id: "4"
      }, {
        name: "02: Boston",
        id: "1",
        "isDefault": true
      }, {
        name: "02: Boston : Overstock",
        id: "3"
      }, {
        name: "HQ",
        id: "6"
      }
    ]
  },

  getLocationsAjax: () => {
    const locations = [{
      name: "01: San Francisco",
      id: "2",
      isDefault: true
    }, {
      name: "01: San Francisco : QA Hold",
      id: "5"
    }, {
      name: "01: San Francisco : Receiving Insp.",
      id: "4"
    }, {
      name: "02: Boston",
      id: "1",
      "isDefault": false
    }, {
      name: "02: Boston : Overstock",
      id: "3"
    }, {
      name: "HQ",
      id: "6"
    }];
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(locations);
      }, 1000);
    })
  },

  getOrdersByLocationAjax: (locationId) => {
    const body = { location: locationId };
    return fetch('scriptlet.nl?script=customscript_dsi_svc_ifbylocation&deploy=customdeploy_dsi_svc_ifbylocation&location=' + locationId, {
      method: 'GET',
      mode: 'cors',
      credentials: 'same-origin',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      }
    });
  },


  getOrdersByTranIdAjax: (tranid) => {
  const body = { tranid: tranid };
  return fetch('scriptlet.nl?script=customscript_dsi_svc_ifbytranidorsscc&deploy=customdeploy_dsi_svc_ifbytranidorsscc&tranid=' + tranid, {
    method: 'GET',
    mode: 'cors',
    credentials: 'same-origin',
    headers: {
      'Accept': 'application/json, text/plain, */*',
      'Content-Type': 'application/json'
    }
  });
},

  submitFulfillmentsAjax: (fulfillments) => {
    const body = { transactions: fulfillments };

    return fetch('scriptlet.nl?script=customscript_dsi_svc_set_if_status&deploy=customdeploy_dsi_svc_set_if_status&transactions=' + JSON.stringify(fulfillments), {
      method: 'GET',
      mode: 'cors',
      credentials: 'same-origin',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      }
    });
  }

};

export default api;