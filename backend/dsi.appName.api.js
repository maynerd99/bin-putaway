/**
 *  @NAPIVersion 2.0
 *  @NModuleScope Public
 *  @NScriptType Restlet
 *  @NAmdConfig /SuiteBundles/Bundle 189151/dsi.module.config.json
 */
define(['N/log', 'N/search', 'N/error', 'N/runtime', 'DSI/safeExecute', 'DSI/timerUtil'],
function (log, search, error, runtime, safeExecute, Timer) {
    function post(data) {
        log.debug('incoming data', data);
        var timer = new Timer();
        var response = { isSuccess: true };

        switch (data.action) {
            case 'getLocationBins':
                response.bins = getLocationBins(data);
                break;

            default:
                response = {
                    isSuccess: false,
                    message: 'Invalid action: ' + data.action
                };
        }

        response.elapsedSeconds = timer.getElapsedSeconds();
        log.debug('response', response);
        return response;
    }

    function getLocationBins(data) {
        var results = search.create({
            type: 'bin',
            filters: [
                ['inactive', 'is', 'F'],
            ].concat(dynamicLocationFilter(data)),
            columns: [
                'binnumber',
                search.createColumn({name: 'binnumber', sort: search.Sort.ASC})
            ]
        }).run().getRange({start: 0, end: 1000});

        return (results || []).map(function (result) {
            return {
                id: result.id,
                binNumber: result.getValue({name: 'binnumber'})
            };
        });
    }

    function dynamicLocationFilter(data) {
        if (isMultiLocation()) {
            if (!data.locationId) {
                throw 'Location required in a multi-location configuration'
            }

            return [
                'and', ['location', 'anyof', data.locationId]
            ]
        }
        return [];
    }


    /**
     * Stubbed Support Functions
     */

    function isMultiLocation() {
        return runtime.isFeatureInEffect('LOCATIONS');
    }

    function isSerializedInventory() {
        return runtime.isFeatureInEffect('SERIALIZEDINVENTORY');
    }

    function isLotManagedInventory() {
        return runtime.isFeatureInEffect('LOTNUMBEREDINVENTORY');
    }

    return {
        post: post
    };
});

