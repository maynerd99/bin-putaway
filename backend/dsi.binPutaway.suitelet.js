/**
 * React frontend for Sales Order Ship written in SuiteScript 2.0
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NAmdConfig /SuiteBundles/Bundle 189151/dsi.module.config.json
 */
define(["N/config", "N/file", "N/log", "N/search", "N/url", 'N/runtime', 'DSI/commonUtil','DSI/appHelper'],
  function (config, file, log, search, url, runtime, common, helper) {
    return {
      onRequest: function (context) {
        var appInfo = {
          name: "Bin Putaway",
          frontendJsName: 'dsi.binPutawayFrontend.js',
          restScriptId:"customscript_dsi_appname_api",
          restDeployId:"customdeploy_dsi_appname_api",
          cssLinkApp: common.getFileURL('dsi.binPutawayFrontend.css'),
          cssSpinner: common.getFileURL('dsi.spinner.css'),
          logo57: common.getFileURL('logo-57.png'),
          logo72: common.getFileURL('logo-72.png'),
          logo114: common.getFileURL('logo-114.png'),
          logo144: common.getFileURL('logo-144.png'),
          sprite: common.getFileURL('dsi-direct-sprites.png'),
          appTitle:'Bin Putaway',
        };

        var app = new helper.suitelet_init();
        var nsConfig = app.getConfiguration(runtime.getCurrentUser().id, appInfo.name);
        var locations = nsConfig.locations.map(function(location) {
          return {
            id: Number(location.id),
            name: location.name,
            isDefault: location.isdefault
          }
        });
        // noinspection CheckTagEmptyBody
        var html =  '<!doctype html>' +
          '   <html lang=\"en\">' +
          '       <head>' +
          '           <link rel=\"stylesheet\" type=\"text/css\" href=\"' + appInfo.cssLinkApp + '\">' +
          '           <link rel=\"stylesheet\" type=\"text/css\" href=\"' + appInfo.cssSpinner + '\">' +
          '           <link rel=\"apple-touch-icon\" sizes=\"57x57\" href=\"' + appInfo.logo57 + '\">' +
          '           <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"' + appInfo.logo72 + '\">' +
          '           <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"' + appInfo.logo114 + '\">' +
          '           <link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"' + appInfo.logo144 + '\">' +
          '           <meta charset=\"utf-8\">' +
          '           <meta name="apple-mobile-web-app-title" content="BinXfer">' +
          '           <meta name="apple-mobile-web-app-capable" content="yes">' +
          '           <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\">' +
          '           <style>.icon { background-image: url(' + appInfo.sprite + '); }</style>' +
          '           <title>' + appInfo.name + '</title>' +
          '           <script>' +
//          '               window.endpoint = \"' + app.getRestletUrl(appInfo.restScriptId,appInfo.restDeployId) + '\";' +
          '               window.locations = ' + JSON.stringify(locations) + ';' +
          '               window.logo = ' + JSON.stringify(common.getPageLogo()) + ';' +
          '               window.back = ' + JSON.stringify(common.getBackButton()) + ';' +
          '               window.menu = ' + JSON.stringify(common.getNavButton()) + ';' +
          '               window.nsConfig = ' + JSON.stringify(nsConfig) + ';' +
          '               window.appTitle = ' + JSON.stringify(appInfo.appTitle) + ';' +
          '           </script>' +
          '       </head>' +
          '       <body style=\"margin: 0; height: calc(100%-60px); overflow-y:scroll;\">' +
          '           <div id=\"root\"></div>' +
          '       </body>' +
          '       <script>' + getFileContents(appInfo.frontendJsName) + '</script>' +
          '   </html>';

        context.response.write(html);

        //////////////////////////////////////////////////////////

        function getFileContents(filename){
          var files = search.create({
            type: 'file',
            filters: [
              ['name', 'is', filename]
            ]
          }).run().getRange({start: 0, end: 1});

          return (files || []).reduce(function(contents, fileRecord) {
            return file.load({id: fileRecord.id}).getContents();
          }, '');
        }
      }
    };
  });
