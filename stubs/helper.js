let helper = {
    suitelet_init() {
        return {
            getAllLocations: function() {},
            getConfiguration: function() {
              return {
                user: { locations: null},
                globals: { default_location: null }
              }
            },
            getRestletUrl: function() {},
            getJS: function() {},
            getCSS: function() {}
        }
    }
};